// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDC_lGjN2OaKdQ6ogp4UjLwK23u7CjXJ1E",
    authDomain: "fir-project-54d5b.firebaseapp.com",
    databaseURL: "https://fir-project-54d5b.firebaseio.com",
    projectId: "fir-project-54d5b",
    storageBucket: "fir-project-54d5b.appspot.com",
    messagingSenderId: "563728651187",
    appId: "1:563728651187:web:093acd1998bec184a8b1c6",
    measurementId: "G-61S17VFN28"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
